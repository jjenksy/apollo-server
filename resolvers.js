
module.exports =  {
    Query: {
        sessions: (parent,args, {dataSources}, info) => {
            return dataSources.sessionApi.getSessions(args);
        },
        sessionById: (parent, {id},{dataSources}, info) => {
            return dataSources.sessionApi.getSessionById(id);
        }
    }
};