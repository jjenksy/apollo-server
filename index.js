const {ApolloServer} = require('apollo-server');
const SessionApi = require('./datasource/session');
// A schema is a collection of type definitions (hence "typeDefs")
// that together define the "shape" of queries that are executed against
// your data.
const typeDefs = require('./schema.js');

const resolvers = require('./resolvers.js');

const dataSources = ()=> ({
   sessionApi: new SessionApi()
});
// The ApolloServer constructor requires two parameters: your schema
// definition and your set of resolvers.
const server = new ApolloServer({ typeDefs, resolvers, dataSources });


server.listen({port: process.env.PORT || 8080}).then(value => {
   console.log(`GraphQL server running @ ${value}`);
});