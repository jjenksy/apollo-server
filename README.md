

## Containerize the app
```
docker build -t jjenksy/graphql-api . 
```

## Run the image
```
docker run -p 4000:4000 -d jjenksy/graphql-api
```

## Build based on Medium article with modified Dockerfile
See [Reference](https://medium.com/google-cloud/deploy-to-cloud-run-using-gitlab-ci-e056685b8eeb
).
