const {gql} = require('apollo-server');

module.exports = gql`
    # Comments in GraphQL strings (such as this one) start with the hash (#) symbol.

    # This "Session" type defines the queryable fields for every Session in our data source.
    type Session {
        id: ID,
        title: String,
        description: String,
        startAt: String,
        endsAt: String,
        room: String,
        day: String,
        format: String,
        track: String,
        level : String
    }

    # The "Query" type is special: it lists all of the available queries that
    # clients can execute, along with the return type for each. In this
    # case, the "books" query returns an array of zero or more Books (defined above).
    type Query {
        sessions(
            id: ID,
            title: String,
            description: String,
            startAt: String,
            endsAt: String,
            room: String,
            day: String,
            format: String,
            track: String,
            level : String): [Session],
        sessionById(id:ID): Session
    }
`;